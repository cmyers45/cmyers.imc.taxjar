﻿using System;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;

namespace IMC.Taxjar
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("[0] Get Order Tax Total");
                Console.WriteLine("[1] Get Location Tax Rate");
                string consoleInput = Console.ReadLine();

                switch (consoleInput)
                {
                    case "0":
                        GetOrderTaxTotal();
                        break;
                    case "1":
                        GetLocationTaxRate();
                        break;
                    default:
                        Console.WriteLine("Invalid Input");
                        break;
                }
            }
            catch (Exception ex)
            {
                // TODO: Write exceptions to log file.
                Console.WriteLine(ex.Message);
            }
        }

        private static void GetOrderTaxTotal()
        {
            string taxOrderPath = ConfigurationManager.AppSettings.Get("tax-order-path");

            StreamReader reader = new StreamReader(taxOrderPath);
            string orderJsonString = reader.ReadToEnd();
            TaxOptions orderTaxOptions = JsonConvert.DeserializeObject<TaxOptions>(orderJsonString);

            TaxJarCalculator calculation1 = new TaxJarCalculator(orderTaxOptions);
            double totalTax = calculation1.GetTaxesForOrder();

            Console.WriteLine($"Total Tax: {totalTax}");
        }

        private static void GetLocationTaxRate()
        {
            string taxLocationPath = ConfigurationManager.AppSettings.Get("tax-location-path");

            StreamReader rateReader = new StreamReader(taxLocationPath);
            string rateJsonString = rateReader.ReadToEnd();
            RateOptions rateTaxOptions = JsonConvert.DeserializeObject<RateOptions>(rateJsonString);

            TaxJarCalculator calculation2 = new TaxJarCalculator(rateTaxOptions);
            string locationRate = calculation2.GetRatesForLocation();

            Console.WriteLine($"Rate: {locationRate}");
        }
    }
}
