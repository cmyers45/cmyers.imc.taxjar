﻿using System.Configuration;
using Newtonsoft.Json;
using RestSharp;

namespace IMC.Taxjar
{
    public class TaxJarCalculator
    {
        private TaxOptions  _TaxOptions  = new TaxOptions();
        private RateOptions _RateOptions = new RateOptions();

        private double totalTax = 0;

        public TaxJarCalculator(TaxOptions options)
        {
            _TaxOptions.Order = new Item[options.Order.Length];

            for (int i = 0; i < options.Order.Length; i++)
            {
                _TaxOptions.Order[i]              = new Item();
                _TaxOptions.Order[i].amount       = options.Order[i].amount;
                _TaxOptions.Order[i].shipping     = options.Order[i].shipping;
                _TaxOptions.Order[i].from_city    = options.Order[i].from_city;
                _TaxOptions.Order[i].from_state   = options.Order[i].from_state;
                _TaxOptions.Order[i].from_country = options.Order[i].from_country;
                _TaxOptions.Order[i].from_zip     = options.Order[i].from_zip;
                _TaxOptions.Order[i].to_city      = options.Order[i].to_city;
                _TaxOptions.Order[i].to_state     = options.Order[i].to_state;
                _TaxOptions.Order[i].to_zip       = options.Order[i].to_zip;
                _TaxOptions.Order[i].to_country   = options.Order[i].to_country;
            }
        }

        public TaxJarCalculator(RateOptions options)
        {
            _RateOptions = options;
        }

        public double GetTaxesForOrder()
        {
            var appSettings = ConfigurationManager.AppSettings;
            string apiToken = appSettings.Get("taxjar-api-token");

            foreach (var item in _TaxOptions.Order)
            {
                RestClient client = new RestClient("https://api.taxjar.com/sales_tax");
                RestRequest request = new RestRequest(Method.GET);

                request.AddHeader("content-type", "application/json");
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("authorization", $"Token token=\"{apiToken}\"");

                foreach (var field in item.GetType().GetProperties())
                {
                    request.AddParameter(field.Name, field.GetValue(item).ToString());
                }

                IRestResponse restResponse = client.Execute(request);
                string restResponseContent = restResponse.Content;
                TaxResponse taxResponse = JsonConvert.DeserializeObject<TaxResponse>(restResponseContent);
                totalTax = totalTax += taxResponse.amount_to_collect;
            }

            return totalTax;
        }

        public string GetRatesForLocation()
        {
            var appSettings = ConfigurationManager.AppSettings;
            string apiToken = appSettings.Get("taxjar-api-token");

            RestClient client = new RestClient($"https://api.taxjar.com/locations/{_RateOptions.rate.zip}/");
            RestRequest request = new RestRequest(Method.GET);

            request.AddHeader("content-type", "application/json");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", $"Token token=\"{apiToken}\"");

            IRestResponse restResponse = client.Execute(request);
            string restResponseContent = restResponse.Content;
            RateResponse rateResponse  = JsonConvert.DeserializeObject<RateResponse>(restResponseContent);

            return rateResponse.location.combined_rate;
        }
    } 
}
