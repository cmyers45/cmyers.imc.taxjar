﻿using System;

namespace IMC.Taxjar
{
    [Serializable]
    public class RateResponse
    {
        public Location location { get; set; }
    }

    public class Location
    {
        public string zip { get; set; }
        public string city { get; set; }
        public string city_rate { get; set; }
        public string state { get; set; }
        public string state_rate { get; set; }
        public string county { get; set; }
        public string county_rate { get; set; }
        public string combined_district_rate { get; set; }
        public string combined_rate { get; set; }
    }
}
