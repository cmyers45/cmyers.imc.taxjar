﻿using System;

namespace IMC.Taxjar
{
    [Serializable]
    public class TaxOptions
    {
        public Item[] Order { get; set; }
    }

    public class Item
    {
        public int amount { get; set; }
        public double shipping { get; set; }
        public string from_city { get; set; }
        public string from_state { get; set; }
        public string from_country { get; set; }
        public int from_zip { get; set; }
        public string to_city { get; set; }
        public string to_state { get; set; }
        public int to_zip { get; set; }
        public string to_country { get; set; }
    }
}
