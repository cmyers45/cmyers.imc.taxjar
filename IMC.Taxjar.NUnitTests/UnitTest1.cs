using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Configuration;
using System.IO;

namespace IMC.Taxjar.NUnitTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void GetTaxesForOrderTest1()
        {
            string taxOrderPath = @"..\..\..\Json\Order.json";

            StreamReader reader = new StreamReader(taxOrderPath);
            string orderJsonString = reader.ReadToEnd();
            TaxOptions orderTaxOptions = JsonConvert.DeserializeObject<TaxOptions>(orderJsonString);

            TaxJarCalculator calculation1 = new TaxJarCalculator(orderTaxOptions);
            double totalTax = calculation1.GetTaxesForOrder();

            Assert.AreEqual(3.2999999999999998, totalTax);
        }

        [Test]
        public void GetTaxesFromLocation1()
        {
            string taxLocationPath = @"..\..\..\Json\Rate_List.json";

            StreamReader rateReader = new StreamReader(taxLocationPath);
            string rateJsonString = rateReader.ReadToEnd();
            RateOptions rateTaxOptions = JsonConvert.DeserializeObject<RateOptions>(rateJsonString);

            TaxJarCalculator calculation2 = new TaxJarCalculator(rateTaxOptions);
            string locationRate = calculation2.GetRatesForLocation();

            Assert.AreEqual(0.07, locationRate);
        }
    }
}